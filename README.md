# Laporan Praktikum Sistem Operasi - Modul 4

## Soal 1

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). *Tagline* **#YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪)** sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran.

Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.

Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

1. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat *file* bernama **storage.c**. Oleh karena itu, *download dataset* tentang pemain sepak bola dari Kaggle. *Dataset* ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan *command* ini untuk men-*download* *dataset*.
    
    **kaggle datasets download -d bryanb/fifa-player-stats-database**
    
    Setelah berhasil men-*download* dalam *format* **.zip**, langkah selanjutnya adalah mengekstrak *file* tersebut.  Kalian melakukannya di dalam *file* **storage.c** untuk semua pengerjaannya.
    
2. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca *file* CSV khusus bernama **FIFA23_official_data.csv** dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain **selain Manchester City**. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam *file* **storage.c** untuk analisis ini.
3. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-*distribute* dan dijalankan di lingkungan lain tanpa perlu *setup environment* dari awal. Buatlah **Dockerfile** yang berisi semua langkah yang diperlukan untuk *setup environment* dan menentukan bagaimana aplikasi harus dijalankan.
    
    Setelah **Dockerfile** berhasil dibuat, langkah selanjutnya adalah membuat **Docker Image**. Gunakan **Docker CLI** untuk mem-*build* *image* dari **Dockerfile** kalian. Setelah berhasil membuat *image*, verifikasi bahwa *image* tersebut berfungsi seperti yang diharapkan dengan menjalankan **Docker Container** dan memeriksa *output*-nya.
    
4. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para *scouting*nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para *scout* dapat mengakses dan menggunakan sistem yang telah diciptakan?
    
    Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-*publish* **Docker Image** sistem ke **Docker Hub**, sebuah layanan *cloud* yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. *Output* dari pekerjaan ini adalah *file* Docker kalian bisa dilihat secara *public* pada **[https://hub.docker.com/r/{Username}/storage-app](https://hub.docker.com/r/%7BUsername%7D/storage-app)**.
    
5. Berita tagline **#YBBA (#YangBiruBiruAja)** semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan **Docker Compose** dengan ***instance* sebanyak 5**. Buat *folder* terpisah bernama **Barcelona** dan **Napoli** dan jalankan **Docker Compose** di sana.

**Catatan:**

- Cantumkan *file* **hubdocker.txt** yang berisi **URL Docker Hub** kalian (public).
- Perhatikan ***port*** pada masing-masing ***instance***.

### **1.1**

Kami diminta untuk membuat program C dengan nama strage**.c**, yang berisi program untuk men-*download dataset* tentang pemain sepak bola dari Kaggle dengan menggunakan *command “**kaggle datasets download -d bryanb/fifa-player-stats-database”.*** Setelah berhasil men-*download* dalam *format* **.zip**, langkah selanjutnya adalah mengekstrak *file* tersebut

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    
    #define MAX_PLAYERS 1000
    #define MAX_LINE_LENGTH 1000
    #define MAX_FIELD_LENGTH 100
    ```
    
    - **`#include <stdio.h>`**: Direktif ini memasukkan header file **`stdio.h`** yang berisi definisi fungsi standar input/output seperti **`printf`**, **`fgets`**, dan **`fopen`**.
    - **`#include <stdlib.h>`**: Direktif ini memasukkan header file **`stdlib.h`** yang berisi definisi fungsi standar untuk alokasi memori dan fungsi **`system`**.
    - **`#include <string.h>`**: Direktif ini memasukkan header file **`string.h`** yang berisi definisi fungsi standar untuk manipulasi string seperti **`strcpy`** dan **`strtok`**.
    - **`#define`**:
        - **`MAX_PLAYERS 1000`**: Ini mendefinisikan konstanta **`MAX_PLAYERS`** dengan nilai 1000. Konstanta ini digunakan untuk menentukan batas maksimum jumlah pemain yang dapat diproses dalam program.
        - **`MAX_LINE_LENGTH 1000`**: Ini mendefinisikan konstanta **`MAX_LINE_LENGTH`** dengan nilai 1000. Konstanta ini digunakan untuk menentukan panjang maksimum sebuah baris yang dapat dibaca dari file.
        - **`MAX_FIELD_LENGTH 100`**: Ini mendefinisikan konstanta **`MAX_FIELD_LENGTH`** dengan nilai 100. Konstanta ini digunakan untuk menentukan panjang maksimum string untuk setiap kolom dalam dataset.
- Pendefinisian Struktur Data
    
    ```c
    typedef struct {
        char name[MAX_FIELD_LENGTH];
        char club[MAX_FIELD_LENGTH];
        int age;
        int potential;
        char photo_url[MAX_FIELD_LENGTH];
    } Player;
    ```
    
    - Di sini, kita mendefinisikan struktur data **`Player`** menggunakan kata kunci **`struct`**. Struktur ini memiliki beberapa field yang mewakili informasi pemain sepak bola, seperti **`name`**, **`club`**, **`age`**, **`potential`**, dan **`photo_url`**. Setiap field memiliki tipe data yang sesuai.
- Proses Download dan Ekstraksi Dataset:
    
    ```c
    int main() {
    
        // download and extract dataset
        system("kaggle datasets download -d bryanb/fifa-player-stats-database");
        system("unzip fifa-player-stats-database.zip");
    ```
    
    - Pada bagian ini, program menggunakan fungsi **`system()`** untuk menjalankan perintah shell yang mengunduh dataset dari Kaggle dengan menggunakan perintah **`kaggle datasets download -d bryanb/fifa-player-stats-database`**.

### **1.2**

Kami diminta untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. kita perlu membaca *file* CSV khusus bernama **FIFA23_official_data.csv** dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain **selain Manchester City**. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam *file* **storage.c** untuk analisis ini.

- Membuka File Dataset
    
    ```c
    FILE *file = fopen("FIFA23_official_data.csv", "r");
        if (file == NULL) {
            printf("File not found.\n");
            return 1;
        }
    ```
    
    - Pada bagian ini, program membuka file "FIFA23_official_data.csv" menggunakan fungsi **`fopen`** dengan mode "r" (read-only) yang mengizinkan hanya operasi bacaan.
    - Jika file tidak dapat ditemukan, program mencetak pesan kesalahan "File not found." dan mengembalikan nilai 1 untuk menandakan keluar dari program.
- Inisialisasi Variabel
    
    ```c
    Player players[MAX_PLAYERS];
        char line[MAX_LINE_LENGTH];
        char field[MAX_FIELD_LENGTH];
    ```
    
    - Program mendeklarasikan beberapa variabel untuk digunakan nanti:
        - **`players`** adalah sebuah array berukuran **`MAX_PLAYERS`** yang bertipe **`Player`**. Ini akan digunakan untuk menyimpan data pemain yang diproses.
        - **`line`** adalah sebuah array karakter berukuran **`MAX_LINE_LENGTH`**. Ini digunakan untuk menyimpan baris data yang dibaca dari file.
        - **`field`** adalah sebuah array karakter berukuran **`MAX_FIELD_LENGTH`**. Ini digunakan untuk menyimpan setiap kolom data saat memproses baris.
- Mengabaikan Baris Header
    
    ```c
    // Skip the header line
        fgets(line, sizeof(line), file);
    ```
    
    - Program menggunakan fungsi **`fgets`** untuk membaca baris pertama dari file dataset yang berisi header. Baris ini diabaikan karena hanya berisi label kolom.
- Membaca dan Memproses Data Pemain
    
    ```c
    int playerCount = 0;
        while (fgets(line, sizeof(line), file)) {
            if (playerCount >= MAX_PLAYERS) {
                printf("Maximum player limit reached.\n");
                break;
            }
    
            int fieldCount = 0;
            char *token = strtok(line, ",");
            while (token != NULL) {
                switch (fieldCount) {
                    case 1:
                        strcpy(players[playerCount].name, token);
                        break;
                    case 8:
                        strcpy(players[playerCount].club, token);
                        break;
                    case 2:
                        players[playerCount].age = atoi(token);
                        break;
                    case 7:
                        players[playerCount].potential = atoi(token);
                        break;
                    case 3:
                        strcpy(players[playerCount].photo_url, token);
                        break;
                }
                token = strtok(NULL, ",");
                fieldCount++;
            }
    
            if (players[playerCount].age < 25 && players[playerCount].potential > 85 && strcmp(players[playerCount].club, "Manchester City") != 0) {
                // Print player information
                printf("Name: %s\n", players[playerCount].name);
                printf("Club: %s\n", players[playerCount].club);
                printf("Age: %d\n", players[playerCount].age);
                printf("Potential: %d\n", players[playerCount].potential);
                printf("Photo URL: %s\n", players[playerCount].photo_url);
                printf("----------------------\n");
            }
    
            playerCount++;
        }
    ```
    
    - Program menggunakan loop **`while`** untuk membaca baris demi baris dari file dataset menggunakan fungsi **`fgets`**.
    - Di dalam loop, program memeriksa apakah jumlah pemain (**`playerCount`**) telah mencapai batas maksimum (**`MAX_PLAYERS`**). Jika iya, program mencetak pesan "Maximum player limit reached." dan keluar dari loop.
    - Program menggunakan fungsi **`strtok`** untuk memecah baris menjadi token-token berdasarkan delimiter koma (",").
    - Setelah itu, program menggunakan pernyataan **`switch-case`** untuk menyimpan nilai dari setiap kolom pada struct **`players[playerCount]`** berdasarkan posisi kolom data:
        - Case 1: Token diambil dan disalin ke field **`name`** pada pemain saat ini menggunakan **`strcpy`**.
        - Case 8: Token diambil dan disalin ke field **`club`** pada pemain saat ini menggunakan **`strcpy`**.
        - Case 2: Token diubah menjadi bilangan bulat menggunakan **`atoi`** dan disimpan pada field **`age`** pada pemain saat ini.
        - Case 7: Token diubah menjadi bilangan bulat menggunakan **`atoi`** dan disimpan pada field **`potential`** pada pemain saat ini.
        - Case 3: Token diambil dan disalin ke field **`photo_url`** pada pemain saat ini menggunakan **`strcpy`**.
    - Setelah itu, program menggunakan **`strtok`** lagi untuk mengambil token berikutnya dan melanjutkan memproses kolom-kolom berikutnya. Penanda kolom (**`fieldCount`**) ditingkatkan pada setiap iterasi.
    - Setelah semua kolom selesai diproses, program memeriksa apakah pemain saat ini memenuhi kriteria umur di bawah 25 tahun, potensi di atas 85, dan bukan bermain di klub "Manchester City". Jika iya, informasi pemain tersebut dicetak.
    - Jumlah pemain (**`playerCount`**) ditingkatkan setiap kali sebuah baris pemain selesai diproses.
- Menutup File dan Mengakhiri Program
    
    ```c
    fclose(file);
        return 0;
    }
    ```
    
    - Program menggunakan fungsi **`fclose`** untuk menutup file yang sudah selesai dibaca.
    - Setelah itu, program mengembalikan nilai 0 sebagai tanda bahwa program telah berakhir dengan sukses.

### **1.3**

Kami diminta untuk membuat **Dockerfile** yang berisi semua langkah yang diperlukan untuk *setup environment* dan menentukan bagaimana aplikasi harus dijalankan. Setelah **Dockerfile** berhasil dibuat, langkah selanjutnya adalah membuat **Docker Image**. Gunakan **Docker CLI** untuk mem-*build* *image* dari **Dockerfile** kalian. Setelah berhasil membuat *image*, verifikasi bahwa *image* tersebut berfungsi seperti yang diharapkan dengan menjalankan **Docker Container** dan memeriksa *output*-nya.

- Membuat Dockerfile
    
    ```docker
    # Menggunakan base image gcc:latest yang berisi kompiler GCC.
    FROM gcc:latest
    
    # Memperbarui daftar paket dan menginstal dependensi yang diperlukan
    RUN apt-get update && \
        apt-get install -y \
        unzip \
        python3 \
        python3-pip \
        python3.11-venv
    
    # Mengatur working directory menjadi /app
    WORKDIR /app
    
    # Menyalin file storage.c ke dalam container di direktori /app
    COPY storage.c /app
    
    # Membuat virtual environment menggunakan Python 3
    RUN python3 -m venv env
    
    # Mengatur PATH agar mencakup direktori bin dari virtual environment
    ENV PATH="/app/env/bin:$PATH"
    
    # Menginstal Kaggle CLI menggunakan pip3
    RUN pip3 install kaggle
    
    # Mengatur environment variable KAGGLE_USERNAME dan KAGGLE_KEY
    ENV KAGGLE_USERNAME=annisarahmapuri
    ENV KAGGLE_KEY=hahahahahahahahahaha
    
    # Menyalin file kredensial Kaggle API (kaggle.json) ke dalam direktori /root/.kaggle
    COPY kaggle.json /root/.kaggle/kaggle.json
    
    # Mengatur izin file kredensial Kaggle API
    RUN chmod 600 /root/.kaggle/kaggle.json
    
    # Mengompilasi program storage.c menjadi file biner storage dengan menggunakan GCC
    RUN gcc storage.c -o storage
    
    # Mengatur perintah default untuk menjalankan program saat container dijalankan
    CMD ["./storage"]
    ```
    
    - Dalam Dockerfile tersebut, kita menggunakan base image gcc:latest yang berisi kompiler GCC. Kemudian kita memperbarui daftar paket dan menginstal dependensi yang diperlukan seperti unzip, python3, python3-pip, dan python3.11-venv.
    - Selanjutnya, kita menyalin file storage.c ke dalam container, membuat virtual environment menggunakan Python 3, menginstal Kaggle CLI, dan menyalin file kredensial Kaggle API ke dalam direktori yang tepat.
    - Setelah itu, kita mengompilasi program storage.c menjadi file biner storage dengan menggunakan GCC. Terakhir, kita menentukan perintah default untuk menjalankan program storage saat container dijalankan. Dockerfile ini memungkinkan kita untuk membangun sebuah Docker image yang berisi aplikasi dan lingkungan yang diperlukan, sehingga dapat didistribusikan dan dijalankan di lingkungan lain dengan mudah.
- Membuat Docker Image
    
    Untuk membangun Docker image, Anda dapat menggunakan perintah berikut di Docker CLI :
    
    ```c
    docker build -t storage-app .
    ```
    
    - Perintah di atas akan membangun Docker image dengan nama "myapp" menggunakan Dockerfile yang ada dalam direktori saat ini (**`.`**).
    
    Setelah proses build selesai, Anda dapat memverifikasi apakah image tersebut berfungsi dengan menjalankan Docker container dari image yang telah dibuat :
    
    ```c
    docker run storage-app
    ```
    
    - Perintah di atas akan menjalankan Docker container dari image "myapp" dan akan menjalankan program **`storage`**.

### **1.4**

Kami diminta untuk mem-*publish* **Docker Image** sistem ke **Docker Hub**, sebuah layanan *cloud* yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. *Output* dari pekerjaan ini adalah *file* Docker kalian bisa dilihat secara *public* pada **[https://hub.docker.com/r/{Username}/storage-app](https://hub.docker.com/r/%7BUsername%7D/storage-app)**

- Proses publish Docker Image
    - jalankan perintah di bawah ini  di terminal untuk membangun Docker image dan memberinya tag yang sesuai dengan username Docker Hub
        
        ```docker
        docker build -t {Username}/storage-app .
        ```
        
    - Setelah proses build selesai, jalankan perintah berikut untuk mengunggah Docker image ke Docker Hub
        
        ```c
        docker push {Username}/storage-app
        ```
        
- Pastikan sudah terpublish
    - Tunggu hingga proses upload selesai. Setelah itu, Docker Image sistem Anda akan tersedia secara public di **[https://hub.docker.com/r/{Username}/storage-app](https://hub.docker.com/r/%7BUsername%7D/storage-app)**, di mana **`{Username}`** adalah nama pengguna Docker Hub Anda.
    - Sesuai permintaan soal, link tersebut dimasukan ke hubdocker.txt
    
    ```c
    https://hub.docker.com/r/anrahmapuri/storage-app
    ```
    

### **1.5**

Kami diminta untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

- File Docker Compose
    
    ```docker
    version: '3'
    services:
      barcelona-storage:
        image: anrahmapuri/storage-app
        deploy:
          replicas: 5
        ports:
          - 8080-8084:8080
    
      napoli-storage:
        image: anrahmapuri/storage-app
        deploy:
          replicas: 5
        ports:
          - 8085-8089:8080
    ```
    
    - Dalam **`docker-compose.yml`** di atas, terdapat dua layanan, yaitu **`barcelona-storage`** dan **`napoli-storage`**. Setiap layanan dijalankan dengan 5 instance menggunakan image **`anrahmapuri/storage-app`**. Port 8080-8084 di-host akan diarahkan ke port 8080 pada kontainer untuk **`barcelona-storage`**, sementara port 8085-8089 akan diarahkan ke port 8080 pada kontainer untuk **`napoli-storage`**. Dengan menggunakan Docker Compose, layanan dapat dijalankan dengan skala yang ditentukan dan dapat diakses melalui port yang telah ditentukan pada host.

## Output

### Storage.c
![Alt Text](https://i.ibb.co/ScB44vv/Screenshot-2023-06-03-193307.png)

### Dockerfile
![Alt Text](https://i.ibb.co/Xxqdsrq/Screenshot-2023-06-03-194927.png)

### Hub.Docker
![Alt Text](https://i.ibb.co/3m0c1w3/Whats-App-Image-2023-05-26-at-00-45-33.jpg)

### Docker Compose
![Alt Text](https://i.ibb.co/GxSRYvC/Screenshot-2023-06-03-195549.png)

## Soal 4

Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 

Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut:

- Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
- Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
Format untuk logging yaitu sebagai berikut.
**[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]**

    **Contoh:
REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
FLAG::230419-12:29:33::RMDIR::/bsi23**

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.
1. Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
2. Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
3. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
4. Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
**Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).**
5. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

### **4.1**

Kami diminta untuk membuat program C dengan nama **modular.c**, yang berisi program untuk  modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Juga membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem dengan ketentuan yang sudah disebutkan.

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #define FUSE_USE_VERSION 31

    #include <fuse.h>
    #include <stdio.h>
    #include <string.h>
    #include <errno.h>
    #include <fcntl.h>
    #include <unistd.h>
    #include <dirent.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <time.h>
    #include <pwd.h>
    #include <stdlib.h>

    #define LOG_FILE_PATH "/home/asxklm/fs_module.log"
    #define CHUNK_SIZE 1024

    ```

- Membuat funngsi mencatat aktivitas log
    
    ```c
    // Fungsi untuk mencatat aktivitas ke file log
    void logActivity(const char *level, const char *cmd, const char *desc) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm);

    FILE *logFile = fopen(LOG_FILE_PATH, "a");
    if (logFile == NULL) {
        perror("Error opening log file");
        return;
    }

    fprintf(logFile, "[%s]::[%s]::[%s]::[%s ...]\n", level, timestamp, cmd, desc);
    fclose(logFile);
    }

    ```
    
    - Fungsi logActivity menerima tiga parameter berupa string konstan (const char *), yaitu level, cmd, dan desc. Parameter level digunakan untuk menentukan tingkat aktivitas, cmd adalah string yang berisi perintah yang dilakukan, dan desc adalah deskripsi atau keterangan tambahan mengenai aktivitas tersebut.
    - Pertama, fungsi ini mendeklarasikan variabel t dengan tipe data time_t untuk menyimpan waktu saat ini.
    - Selanjutnya, fungsi menggunakan fungsi localtime untuk mengubah nilai t menjadi struktur tm yang berisi informasi waktu terperinci seperti tahun, bulan, hari, jam, dan lain sebagainya.
    - Variabel timestamp dengan tipe array karakter sebesar 20 dideklarasikan untuk menyimpan timestamp yang akan digunakan dalam log. Fungsi strftime digunakan untuk mengisi nilai timestamp dengan format waktu yang diinginkan, yaitu "%y%m%d-%H:%M:%S" (misalnya, "210603-14:30:15" untuk tanggal 3 Juni 2021 pukul 14:30:15).
    - Selanjutnya, fungsi membuka file log dengan mode "a" (append) menggunakan fungsi fopen dan menyimpan pointer file dalam variabel logFile. Jika gagal membuka file log, maka fungsi akan menampilkan pesan kesalahan dengan perror dan mengembalikan fungsi.
    - Kemudian, menggunakan fprintf, fungsi menulis entri log ke dalam file dengan format yang ditentukan, yaitu "[%s]::[%s]::[%s]::[%s ...]\n". Parameter level, timestamp, cmd, dan desc akan digunakan untuk mengisi nilai dalam format tersebut.
    - Setelah selesai menulis, fungsi menutup file log menggunakan fclose.

### **4.2**

Kami diminta untuk saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

- Membuat fingsi membaca direktori dan melakukan fuse didalamnya
    
    ```c
    // Fungsi untuk membaca direktori
    static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                      off_t offset, struct fuse_file_info *fi) {
    DIR *dir;
    struct dirent *entry;

    (void)offset;
    (void)fi;

    dir = opendir(path);
    if (dir == NULL)
        return -errno;

    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0)
            break;
    }

    closedir(dir);
    return 0;
    }

    ```
    - Fungsi fs_readdir merupakan fungsi dengan tipe int yang menerima beberapa parameter, yaitu path (path dari direktori yang akan dibaca), buf (buffer untuk menyimpan hasil bacaan), filler (fungsi yang digunakan untuk memasukkan entri direktori ke dalam buffer), offset (offset dalam direktori yang akan dibaca), dan fi (informasi file yang terkait).
    - Variabel dir dan entry digunakan untuk menyimpan pointer ke direktori dan entri direktori yang sedang diproses.
    - Parameter offset dan fi tidak digunakan dalam fungsi ini, sehingga digunakan (void) untuk mengabaikannya dan mencegah timbulnya peringatan tidak terpakai.
    - Fungsi opendir digunakan untuk membuka direktori dengan path yang diberikan. Jika direktori gagal dibuka, fungsi akan mengembalikan nilai -errno yang menunjukkan adanya kesalahan.
    - Dalam sebuah perulangan, fungsi menggunakan readdir untuk membaca entri-entri dari direktori yang sedang dibuka. Setiap entri direktori yang terbaca akan dimasukkan ke dalam buffer menggunakan fungsi filler. Jika fungsi filler mengembalikan nilai bukan 0, perulangan akan dihentikan.
    - Setelah selesai membaca entri-entri direktori, fungsi closedir digunakan untuk menutup direktori yang telah dibuka.
    - Fungsi mengembalikan nilai 0 untuk menandakan bahwa proses membaca direktori selesai tanpa adanya kesalahan.
    
### **4.3**

Kami diminta untuk membuat Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log).

- Define Path log
    
    ```c
    #define LOG_FILE_PATH "/home/asxklm/fs_module.log"
    ```
    
    ```c
    // Fungsi untuk mendapatkan atribut file
    static int fs_getattr(const char *path, struct stat *stbuf) {
    int res;

    res = lstat(path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
    }

    // Fungsi untuk membaca direktori
    static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                      off_t offset, struct fuse_file_info *fi) {
    DIR *dir;
    struct dirent *entry;

    (void)offset;
    (void)fi;

    dir = opendir(path);
    if (dir == NULL)
        return -errno;

    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0)
            break;
    }

    closedir(dir);
    return 0;
    }

    // Fungsi untuk membuka file
    static int fs_open(const char *path, struct fuse_file_info *fi) {
    int res;

    res = open(path, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    return 0;
    }

    // Fungsi untuk membuat file
    static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int res;

    res = creat(path, mode);
    if (res == -1)
        return -errno;

    close(res);
    logActivity("REPORT", "create", path);
    return 0;
    }

    // Fungsi untuk mengubah nama file atau direktori
    static int fs_rename(const char *oldpath, const char *newpath) {
    int res;

    res = rename(oldpath, newpath);
    if (res == -1)
        return -errno;

    logActivity("REPORT", "rename", oldpath);
    return 0;
    }

    // Fungsi untuk membuat direktori
    static int fs_mkdir(const char *path, mode_t mode) {
    int res;

    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    logActivity("REPORT", "mkdir", path);
    return 0;
    }

    // Fungsi untuk menghapus direktori
    static int fs_rmdir(const char *path) {
    int res;

    res = rmdir(path);
    if (res == -1)
        return -errno;

    logActivity("FLAG", "rmdir", path);
    return 0;
    }

    // Fungsi untuk menghapus file
    static int fs_unlink(const char *path) {
    int res;

    res = unlink(path);
    if (res == -1)
        return -errno;

    logActivity("FLAG", "unlink", path);
    return 0;
    }

    ```
    - Pada bagian ini, Mendeklarasikan fungsi-fungsi yang akan dipanggil saat melakukan fuse dan dicatat aktivitasnya pada log.

### **4.4**

Kami diminta untuk menjadikan file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

- Proses Modularisasi:
    
    ```c
    // Fungsi untuk membagi file menjadi beberapa bagian
    void splitFile(const char *filePath) {
    FILE *inputFile = fopen(filePath, "rb");
    if (inputFile == NULL) {
        perror("Error opening input file");
        return;
    }

    char *buffer = (char *)malloc(CHUNK_SIZE);
    if (buffer == NULL) {
        perror("Error allocating memory");
        fclose(inputFile);
        return;
    }

    int chunkNumber = 0;
    size_t bytesRead;
    while ((bytesRead = fread(buffer, 1, CHUNK_SIZE, inputFile)) > 0) {
        char chunkFilePath[256];
        sprintf(chunkFilePath, "%s.%d", filePath, chunkNumber);

        FILE *chunkFile = fopen(chunkFilePath, "wb");
        if (chunkFile == NULL) {
            perror("Error creating chunk file");
            free(buffer);
            fclose(inputFile);
            return;
        }

        fwrite(buffer, 1, bytesRead, chunkFile);
        fclose(chunkFile);

        chunkNumber++;
    }

    free(buffer);
    fclose(inputFile);
    }

    ```
    
    - Fungsi splitFile menerima parameter filePath yang merupakan path file yang akan dibagi.
    - Pertama, fungsi membuka file input menggunakan fopen dengan mode "rb" (read-binary). Jika file input gagal dibuka, fungsi akan menampilkan pesan kesalahan dengan perror dan mengembalikan fungsi.
    - Selanjutnya, fungsi mengalokasikan memori untuk buffer dengan ukuran CHUNK_SIZE menggunakan malloc. Jika alokasi memori gagal, fungsi akan menampilkan pesan kesalahan dengan perror, menutup file input menggunakan fclose, dan mengembalikan fungsi.
    - Variabel chunkNumber diinisialisasi dengan nilai 0.
    - Dalam sebuah perulangan, fungsi membaca CHUNK_SIZE byte dari file input ke dalam buffer menggunakan fread. Jika pembacaan berhasil (bytesRead > 0), maka perulangan akan dijalankan.
    - Pada setiap iterasi perulangan, fungsi membuat path file untuk bagian chunk baru menggunakan sprintf dengan memasukkan filePath dan chunkNumber ke dalam chunkFilePath.
    - Fungsi membuka file chunk baru menggunakan fopen dengan mode "wb" (write-binary). Jika file chunk gagal dibuka, fungsi akan menampilkan pesan kesalahan dengan perror, membebaskan memori buffer menggunakan free, menutup file input menggunakan fclose, dan mengembalikan fungsi.
    - Fungsi menulis bytesRead byte dari buffer ke dalam file chunk menggunakan fwrite.
    - Setelah selesai menulis, fungsi menutup file chunk menggunakan fclose.
    - Variabel chunkNumber ditambah 1 untuk menunjukkan bahwa bagian chunk berikutnya akan memiliki nomor yang berbeda.
    - Perulangan di atas akan terus berlanjut selama masih ada byte yang terbaca dari file input.
    - Setelah selesai membagi file, fungsi membebaskan memori yang dialokasikan untuk buffer menggunakan free dan menutup file input menggunakan fclose.

### **4.5**

Kami diminta Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

- Mudular Fixed
    
    ```c
    // Fungsi untuk menggabungkan file-file chunk menjadi satu file utuh
    void mergeFiles(const char *dirPath, const char *outputFilePath) {
    FILE *outputFile = fopen(outputFilePath, "wb");
    if (outputFile == NULL) {
        perror("Error creating output file");
        return;
    }

    char chunkFilePath[256];
    int chunkNumber = 0;
    sprintf(chunkFilePath, "%s.%d", dirPath, chunkNumber);

    while (access(chunkFilePath, F_OK) != -1) {
        FILE *chunkFile = fopen(chunkFilePath, "rb");
        if (chunkFile == NULL) {
            perror("Error opening chunk file");
            fclose(outputFile);
            return;
        }

        char *buffer = (char *)malloc(CHUNK_SIZE);
        if (buffer == NULL) {
            perror("Error allocating memory");
            fclose(chunkFile);
            fclose(outputFile);
            return;
        }

        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, CHUNK_SIZE, chunkFile)) > 0) {
            fwrite(buffer, 1, bytesRead, outputFile);
        }

        free(buffer);
        fclose(chunkFile);

        chunkNumber++;
        sprintf(chunkFilePath, "%s.%d", dirPath, chunkNumber);
    }

    fclose(outputFile);
    }

    ```
    
    - Fungsi mergeFiles menerima dua parameter, yaitu dirPath yang merupakan path direktori tempat file-file chunk disimpan, dan outputFilePath yang merupakan path tempat file utuh hasil penggabungan akan disimpan.
    - Pertama, fungsi membuka file output menggunakan fopen dengan mode "wb" (write-binary). Jika file output gagal dibuka, maka fungsi akan menampilkan pesan kesalahan dengan perror dan mengembalikan fungsi.
    - Selanjutnya, fungsi mendeklarasikan variabel chunkFilePath dengan tipe array karakter sebesar 256 untuk menyimpan path file chunk. Variabel chunkNumber diinisialisasi dengan nilai 0.
    - Menggunakan sprintf, fungsi membuat path file chunk pertama dengan memasukkan dirPath dan chunkNumber ke dalam chunkFilePath.
    - Dalam sebuah perulangan, fungsi melakukan pengecekan apakah file chunk yang ditunjuk oleh chunkFilePath ada atau tidak menggunakan access dengan parameter F_OK. Jika file chunk ditemukan, maka perulangan akan dijalankan.
    - Pada setiap iterasi perulangan, fungsi membuka file chunk menggunakan fopen dengan mode "rb" (read-binary). Jika file chunk gagal dibuka, maka fungsi akan menampilkan pesan kesalahan dengan perror, menutup file output menggunakan fclose, dan mengembalikan fungsi.
    - Selanjutnya, fungsi mengalokasikan memori untuk buffer dengan ukuran CHUNK_SIZE menggunakan malloc. Jika alokasi memori gagal, maka fungsi akan menampilkan pesan kesalahan dengan perror, menutup file chunk dan file output menggunakan fclose, dan mengembalikan fungsi.
    - Dalam sebuah perulangan, fungsi membaca CHUNK_SIZE byte dari file chunk ke dalam buffer menggunakan fread. Jika pembacaan berhasil (bytesRead > 0), maka fungsi menulis bytesRead byte dari buffer ke dalam file output menggunakan fwrite.
    - Setelah selesai membaca dan menulis, fungsi membebaskan memori yang dialokasikan untuk buffer menggunakan free dan menutup file chunk menggunakan fclose.
    - Variabel chunkNumber ditambahkan 1 dan chunkFilePath diperbarui dengan path file chunk berikutnya menggunakan sprintf.
    - Perulangan di atas akan terus berlanjut selama masih ada file chunk yang ditemukan.
    - Setelah selesai penggabungan, fungsi menutup file output menggunakan fclose.

## Output

### fs_module.log
![Alt Text](https://i.ibb.co/zFFjm2Y/Screenshot-147.png)


## Soal 5
Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.	
-a. Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
-b. Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile
-c. Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
Contoh
- kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
penjelasan
- kyunkyun adalah username.
- fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
catatan:
- Tidak perlu ada password validation untuk mempermudah kalian.	
-d. Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.
-e. List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
Contoh: 
result.txt

### rahasia.c 
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <fuse_opt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

static const char *hidden_folder_path = "/home/testing/shift4/rahasia";
static const char *user_file_path = "/home/testing/shift4/users.txt";
static const char *result_file_path = "/home/testing/shift4/result.txt";
static const char *extension_file_path = "/home/testing/shift4/extension.txt";
static const char *extensions[] = {".gif", ".mp3", ".pdf", ".png", ".jpg", ".txt"};

static const int num_extensions = sizeof(extensions) / sizeof(extensions[0]);
static int is_authenticated = 0;
```
Bagian ini mencakup inklusi header yang diperlukan dan mendefinisikan beberapa variabel konstan yang digunakan dalam program. hidden_folder_path adalah jalur folder tersembunyi yang akan digunakan, user_file_path adalah jalur file yang menyimpan informasi pengguna, result_file_path adalah jalur file yang digunakan untuk menyimpan hasil dari operasi penggantian nama, extension_file_path adalah jalur file untuk menyimpan informasi jumlah file berdasarkan ekstensi, dan extensions adalah array yang berisi ekstensi file yang dihitung.

```c
static int isUserFileExist() {
    if (access(user_file_path, F_OK) != -1) {
        return 1;
    }
    else {
        return 0;
    }
}

char* calculateMD5(const char* str) {
    unsigned char digest[MD5_DIGEST_LENGTH];
    EVP_MD_CTX* mdctx;
    const EVP_MD* md = EVP_md5();
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, str, strlen(str));
    EVP_DigestFinal_ex(mdctx, digest, NULL);
    EVP_MD_CTX_free(mdctx);

    char* hashedStr = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        sprintf(&hashedStr[i * 2], "%02x", (unsigned int)digest[i]);

    return hashedStr;
}

int isFileDownloaded(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file) {
        fclose(file);
        return 1;
    }
    return 0;
}

char downloadAndUnzipFile() {
    if (isFileDownloaded("rahasia.zip")) {
        printf("rahasia.zip already exists.\n");
    } else {
        char url[] = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
        char cmd[200] = "wget -O rahasia.zip '";
        strcat(cmd, url);
        strcat(cmd, "'");
        system(cmd);

        system("rm -rf rahasia");
        system("unzip rahasia.zip");
    }
    return 0;
}
```
Bagian ini berisi beberapa fungsi utilitas yang digunakan dalam program. isUserFileExist digunakan untuk memeriksa apakah file pengguna ada. calculateMD5 digunakan untuk menghitung nilai hash MD5 dari string yang diberikan. isFileDownloaded digunakan untuk memeriksa apakah suatu file telah diunduh. downloadAndUnzipFile digunakan untuk mengunduh dan mengekstrak file rahasia jika belum ada.

```c
static void countFilesRecursive(const char *path, int *fileCount) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Cannot open folder %s.\n", path);
        return;
    }

    struct dirent *de;
    while ((de = readdir(dir)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        char filePath[256];
        sprintf(filePath, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) {
            countFilesRecursive(filePath, fileCount);
        } else if (de->d_type == DT_REG) {
            char *filename = de->d_name;
            char *extension = strrchr(filename, '.');

            if (extension != NULL) {
                for (int i = 0; i < num_extensions; i++) {
                    if (strcmp(extension, extensions[i]) == 0) {
                        fileCount[i]++;
                        break;
                    }
                }
            }
        }
    }

    closedir(dir);
}

static void countFilesByExtension() {
    int fileCount[num_extensions];
    memset(fileCount, 0, sizeof(fileCount));

    countFilesRecursive(hidden_folder_path, fileCount);

    FILE *extension_file = fopen(extension_file_path, "w");
    if (extension_file == NULL) {
        printf("Error: Cannot create extension file.\n");
        return;
    }

    for (int i = 0; i < num_extensions; i++) {
        fprintf(extension_file, "%s: %d\n", extensions[i], fileCount[i]);
    }

    fclose(extension_file);

    printf("File count by extension saved to extension.txt.\n");
}

static int customGetAttr(const char *path, struct stat *stbuf)
{
    int res;
    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    res = lstat(fullPath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int customRename(const char *from, const char *to)
{
    if (!is_authenticated) {
        printf("Please log in first.\n");
        return -1;
    }

    char name[256];
    sscanf(from, "/%[^/]", name);

    char groupCode[256];
    sscanf(to, "/%[^_]", groupCode);

    char newName[256];
    sprintf(newName, "/%s_%s", groupCode, name);

    char oldPath[256];
    sprintf(oldPath, "%s%s", hidden_folder_path, from);

    char newPath[256];
    sprintf(newPath, "%s%s", hidden_folder_path, to);

    struct stat st;
    stat(oldPath, &st);
    int isDirectory = S_ISDIR(st.st_mode);

    int res;
    if (isDirectory) {
        res = rename(oldPath, newPath);
        if (res == -1)
            return -errno;

        DIR *dir = opendir(newPath);
        if (dir == NULL)
            return -errno;

        struct dirent *de;
        while ((de = readdir(dir)) != NULL) {
            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                continue;

            char oldSubPath[256];
            sprintf(oldSubPath, "%s/%s", oldPath, de->d_name);

            char newSubPath[256];
            sprintf(newSubPath, "%s/%s", newPath, de->d_name);

            res = customRename(oldSubPath, newSubPath);
            if (res == -1)
                return -errno;
        }

        closedir(dir);
    } else {
        char parentDir[256];
        strncpy(parentDir, newPath, sizeof(parentDir));
        parentDir[sizeof(parentDir) - 1] = '\0';
        char *parent = dirname(parentDir);
        mkdir(parent, 0755);

        res = rename(oldPath, newPath);
        if (res == -1)
            return -errno;
    }

    FILE *resultFile = fopen(result_file_path, "a");
    if (resultFile == NULL) {
        printf("Error: Cannot open the result file.\n");
        return -1;
    }
    fprintf(resultFile, "%s -> %s\n", oldPath, newPath);
    fclose(resultFile);

    return 0;
}

static int customReaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    DIR *dir = opendir(fullPath);
    if (dir == NULL)
        return -errno;

    struct dirent *de;
    while ((de = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dir);
    return 0;
}

static int customRead(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    int fd = open(fullPath, O_RDONLY);
    if (fd == -1)
        return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static struct fuse_operations custom_oper = {
    .getattr = customGetAttr,
    .readdir = customReaddir,
    .read = customRead,
    .rename = customRename,
};
```
Bagian ini berisi fungsi-fungsi operasi FUSE yang akan dipanggil oleh sistem file virtual. countFilesRecursive adalah fungsi rekursif untuk menghitung jumlah file berdasarkan ekstensi dalam folder tertentu. countFilesByExtension digunakan untuk menghitung jumlah file berdasarkan ekstensi dan menyimpan hasilnya dalam file. Fungsi-fungsi lainnya seperti customGetAttr, customRename, customReaddir, dan customRead adalah fungsi-fungsi yang diimplementasikan untuk menjalankan operasi terkait atribut file, penggantian nama, membaca direktori, dan membaca file.
```c
int main(int argc, char *argv[])
{
    downloadAndUnzipFile();
    umask(0);

    int userFileExist = isUserFileExist();
    if (!userFileExist) {
        printf("User file not found. Creating new user file.\n");
        FILE *userFile = fopen(user_file_path, "w");
        if (userFile == NULL) {
            printf("Error: Cannot create user file.\n");
            return -1;
        }
        fclose(userFile);
    }

    if (argc < 3) {
        printf("Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }

    if (strcmp(argv[2], "-r") == 0) {
        printf("Registering new user...\n");
        registerUser();
        return 0;
    } else if (strcmp(argv[2], "-l") == 0) {
        printf("Logging in...\n");
        if (login() == 0) {
            printf("Login successful. Mounting...\n");

            struct fuse_args args = FUSE_ARGS_INIT(0, NULL);
            fuse_opt_add_arg(&args, argv[0]);
            fuse_opt_add_arg(&args, argv[1]);
            fuse_opt_add_arg(&args, "-o");
            fuse_opt_add_arg(&args, "nonempty");

            countFilesByExtension();

            return fuse_main(args.argc, args.argv, &custom_oper, NULL);
        } else {
            printf("Login failed. Exiting...\n");
            return -1;
        }
    } else {
        printf("Invalid argument. Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }

    return 0;
}
```
Bagian ini adalah fungsi utama dari program. Ini adalah tempat di mana Anda akan memanggil fungsi-fungsi yang relevan untuk melakukan operasi yang diinginkan, seperti registrasi pengguna, login, dan mounting sistem file FUSE.

Setiap bagian dalam kode memiliki tujuan dan tanggung jawabnya sendiri. Dengan membagi kode ke dalam bagian-bagian yang terorganisir, memudahkan pemahaman dan pengelolaan kode secara keseluruhan.

## output

### Register
<a href="https://ibb.co/PjNX4qf"><img src="https://i.ibb.co/Rb9rPRV/Screenshot-123.png" alt="Screenshot-123" border="0"></a>

### Login 
<a href="https://ibb.co/LnrTd7P"><img src="https://i.ibb.co/syCT9YP/Screenshot-125.png" alt="Screenshot-125" border="0"></a>

### Rename 
<a href="https://ibb.co/rxpxYG4"><img src="https://i.ibb.co/N9N956Z/Screenshot-126.png" alt="Screenshot-126" border="0"></a>

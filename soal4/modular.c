#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <pwd.h>
#include <stdlib.h>

#define LOG_FILE_PATH "/home/asxklm/fs_module.log"
#define CHUNK_SIZE 1024

// Fungsi untuk mencatat aktivitas ke file log
void logActivity(const char *level, const char *cmd, const char *desc) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm);

    FILE *logFile = fopen(LOG_FILE_PATH, "a");
    if (logFile == NULL) {
        perror("Error opening log file");
        return;
    }

    fprintf(logFile, "[%s]::[%s]::[%s]::[%s ...]\n", level, timestamp, cmd, desc);
    fclose(logFile);
}

// Fungsi untuk membagi file menjadi beberapa bagian
void splitFile(const char *filePath) {
    FILE *inputFile = fopen(filePath, "rb");
    if (inputFile == NULL) {
        perror("Error opening input file");
        return;
    }

    char *buffer = (char *)malloc(CHUNK_SIZE);
    if (buffer == NULL) {
        perror("Error allocating memory");
        fclose(inputFile);
        return;
    }

    int chunkNumber = 0;
    size_t bytesRead;
    while ((bytesRead = fread(buffer, 1, CHUNK_SIZE, inputFile)) > 0) {
        char chunkFilePath[256];
        sprintf(chunkFilePath, "%s.%d", filePath, chunkNumber);

        FILE *chunkFile = fopen(chunkFilePath, "wb");
        if (chunkFile == NULL) {
            perror("Error creating chunk file");
            free(buffer);
            fclose(inputFile);
            return;
        }

        fwrite(buffer, 1, bytesRead, chunkFile);
        fclose(chunkFile);

        chunkNumber++;
    }

    free(buffer);
    fclose(inputFile);
}

// Fungsi untuk menggabungkan file-file chunk menjadi satu file utuh
void mergeFiles(const char *dirPath, const char *outputFilePath) {
    FILE *outputFile = fopen(outputFilePath, "wb");
    if (outputFile == NULL) {
        perror("Error creating output file");
        return;
    }

    char chunkFilePath[256];
    int chunkNumber = 0;
    sprintf(chunkFilePath, "%s.%d", dirPath, chunkNumber);

    while (access(chunkFilePath, F_OK) != -1) {
        FILE *chunkFile = fopen(chunkFilePath, "rb");
        if (chunkFile == NULL) {
            perror("Error opening chunk file");
            fclose(outputFile);
            return;
        }

        char *buffer = (char *)malloc(CHUNK_SIZE);
        if (buffer == NULL) {
            perror("Error allocating memory");
            fclose(chunkFile);
            fclose(outputFile);
            return;
        }

        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, CHUNK_SIZE, chunkFile)) > 0) {
            fwrite(buffer, 1, bytesRead, outputFile);
        }

        free(buffer);
        fclose(chunkFile);

        chunkNumber++;
        sprintf(chunkFilePath, "%s.%d", dirPath, chunkNumber);
    }

    fclose(outputFile);
}

// Fungsi untuk mendapatkan atribut file
static int fs_getattr(const char *path, struct stat *stbuf) {
    int res;

    res = lstat(path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

// Fungsi untuk membaca direktori
static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                      off_t offset, struct fuse_file_info *fi) {
    DIR *dir;
    struct dirent *entry;

    (void)offset;
    (void)fi;

    dir = opendir(path);
    if (dir == NULL)
        return -errno;

    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0)
            break;
    }

    closedir(dir);
    return 0;
}

// Fungsi untuk membuka file
static int fs_open(const char *path, struct fuse_file_info *fi) {
    int res;

    res = open(path, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    return 0;
}

// Fungsi untuk membuat file
static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int res;

    res = creat(path, mode);
    if (res == -1)
        return -errno;

    close(res);
    logActivity("REPORT", "create", path);
    return 0;
}

// Fungsi untuk mengubah nama file atau direktori
static int fs_rename(const char *oldpath, const char *newpath) {
    int res;

    res = rename(oldpath, newpath);
    if (res == -1)
        return -errno;

    logActivity("REPORT", "rename", oldpath);
    return 0;
}

// Fungsi untuk membuat direktori
static int fs_mkdir(const char *path, mode_t mode) {
    int res;

    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    logActivity("REPORT", "mkdir", path);
    return 0;
}

// Fungsi untuk menghapus direktori
static int fs_rmdir(const char *path) {
    int res;

    res = rmdir(path);
    if (res == -1)
        return -errno;

    logActivity("FLAG", "rmdir", path);
    return 0;
}

// Fungsi untuk menghapus file
static int fs_unlink(const char *path) {
    int res;

    res = unlink(path);
    if (res == -1)
        return -errno;

    logActivity("FLAG", "unlink", path);
    return 0;
}

// Inisialisasi struktur fuse_operations yang berisi fungsi-fungsi yang akan dipanggil oleh FUSE
static struct fuse_operations fs_oper = {
    .getattr    = fs_getattr,
    .readdir    = fs_readdir,
    .open       = fs_open,
    .create     = fs_create,
    .rename     = fs_rename,
    .mkdir      = fs_mkdir,
    .rmdir      = fs_rmdir,
    .unlink     = fs_unlink,
};

// Fungsi utama
int main(int argc, char *argv[]) {
    umask(0);
    return fuse_main(argc, argv, &fs_oper, NULL);
}

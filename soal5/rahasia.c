#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <fuse_opt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <openssl/opensslv.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

static const char *hidden_folder_path = "/home/testing/shift4/rahasia";
static const char *user_file_path = "/home/testing/shift4/users.txt";
static const char *result_file_path = "/home/testing/shift4/result.txt";
static const char *extension_file_path = "/home/testing/shift4/extension.txt";
static const char *extensions[] = {".gif", ".mp3", ".pdf", ".png", ".jpg", ".txt"};

static const int num_extensions = sizeof(extensions) / sizeof(extensions[0]);
static int is_authenticated = 0;

static int isUserFileExist() {
    if (access(user_file_path, F_OK) != -1) {
        return 1;
    }
    else {
        return 0;
    }
}

char* calculateMD5(const char* str) {
    unsigned char digest[MD5_DIGEST_LENGTH];
    EVP_MD_CTX* mdctx;
    const EVP_MD* md = EVP_md5();
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, str, strlen(str));
    EVP_DigestFinal_ex(mdctx, digest, NULL);
    EVP_MD_CTX_free(mdctx);

    char* hashedStr = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        sprintf(&hashedStr[i * 2], "%02x", (unsigned int)digest[i]);

    return hashedStr;
}

int isFileDownloaded(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file) {
        fclose(file);
        return 1;
    }
    return 0;
}

char downloadAndUnzipFile() {
    if (isFileDownloaded("rahasia.zip")) {
        printf("rahasia.zip already exists.\n");
    } else {
        char url[] = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
        char cmd[200] = "wget -O rahasia.zip '";
        strcat(cmd, url);
        strcat(cmd, "'");
        system(cmd);

        system("rm -rf rahasia");
        system("unzip rahasia.zip");
    }
    return 0;
}

static void countFilesRecursive(const char *path, int *fileCount) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Cannot open folder %s.\n", path);
        return;
    }

    struct dirent *de;
    while ((de = readdir(dir)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        char filePath[256];
        sprintf(filePath, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) {
            countFilesRecursive(filePath, fileCount);
        } else if (de->d_type == DT_REG) {
            char *filename = de->d_name;
            char *extension = strrchr(filename, '.');

            if (extension != NULL) {
                for (int i = 0; i < num_extensions; i++) {
                    if (strcmp(extension, extensions[i]) == 0) {
                        fileCount[i]++;
                        break;
                    }
                }
            }
        }
    }

    closedir(dir);
}

static void countFilesByExtension() {
    int fileCount[num_extensions];
    memset(fileCount, 0, sizeof(fileCount));

    countFilesRecursive(hidden_folder_path, fileCount);

    FILE *extension_file = fopen(extension_file_path, "w");
    if (extension_file == NULL) {
        printf("Error: Cannot create extension file.\n");
        return;
    }

    for (int i = 0; i < num_extensions; i++) {
        fprintf(extension_file, "%s: %d\n", extensions[i], fileCount[i]);
    }

    fclose(extension_file);

    printf("File count by extension saved to extension.txt.\n");
}

static int customGetAttr(const char *path, struct stat *stbuf)
{
    int res;
    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    res = lstat(fullPath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int customRename(const char *from, const char *to)
{
    if (!is_authenticated) {
        printf("Please log in first.\n");
        return -1;
    }

    char name[256];
    sscanf(from, "/%[^/]", name);

    char groupCode[256];
    sscanf(to, "/%[^_]", groupCode);

    char newName[256];
    sprintf(newName, "/%s_%s", groupCode, name);

    char oldPath[256];
    sprintf(oldPath, "%s%s", hidden_folder_path, from);

    char newPath[256];
    sprintf(newPath, "%s%s", hidden_folder_path, to);

    struct stat st;
    stat(oldPath, &st);
    int isDirectory = S_ISDIR(st.st_mode);

    int res;
    if (isDirectory) {
        res = rename(oldPath, newPath);
        if (res == -1)
            return -errno;

        DIR *dir = opendir(newPath);
        if (dir == NULL)
            return -errno;

        struct dirent *de;
        while ((de = readdir(dir)) != NULL) {
            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                continue;

            char oldSubPath[256];
            sprintf(oldSubPath, "%s/%s", oldPath, de->d_name);

            char newSubPath[256];
            sprintf(newSubPath, "%s/%s", newPath, de->d_name);

            res = customRename(oldSubPath, newSubPath);
            if (res == -1)
                return -errno;
        }

        closedir(dir);
    } else {
        char parentDir[256];
        strncpy(parentDir, newPath, sizeof(parentDir));
        parentDir[sizeof(parentDir) - 1] = '\0';
        char *parent = dirname(parentDir);
        mkdir(parent, 0755);

        res = rename(oldPath, newPath);
        if (res == -1)
            return -errno;
    }

    FILE *resultFile = fopen(result_file_path, "a");
    if (resultFile == NULL) {
        printf("Error: Cannot open the result file.\n");
        return -1;
    }
    fprintf(resultFile, "%s -> %s\n", oldPath, newPath);
    fclose(resultFile);

    return 0;
}

static int customReaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    DIR *dir = opendir(fullPath);
    if (dir == NULL)
        return -errno;

    struct dirent *de;
    while ((de = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dir);
    return 0;
}

static int customRead(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s%s", hidden_folder_path, path);

    int fd = open(fullPath, O_RDONLY);
    if (fd == -1)
        return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static struct fuse_operations custom_oper = {
    .getattr = customGetAttr,
    .readdir = customReaddir,
    .read = customRead,
    .rename = customRename,
};

int main(int argc, char *argv[])
{
    downloadAndUnzipFile();
    umask(0);

    int userFileExist = isUserFileExist();
    if (!userFileExist) {
        printf("User file not found. Creating new user file.\n");
        FILE *userFile = fopen(user_file_path, "w");
        if (userFile == NULL) {
            printf("Error: Cannot create user file.\n");
            return -1;
        }
        fclose(userFile);
    }

    if (argc < 3) {
        printf("Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }

    if (strcmp(argv[2], "-r") == 0) {
        printf("Registering new user...\n");
        registerUser();
        return 0;
    } else if (strcmp(argv[2], "-l") == 0) {
        printf("Logging in...\n");
        if (login() == 0) {
            printf("Login successful. Mounting...\n");

            struct fuse_args args = FUSE_ARGS_INIT(0, NULL);
            fuse_opt_add_arg(&args, argv[0]);
            fuse_opt_add_arg(&args, argv[1]);
            fuse_opt_add_arg(&args, "-o");
            fuse_opt_add_arg(&args, "nonempty");

            countFilesByExtension();

            return fuse_main(args.argc, args.argv, &custom_oper, NULL);
        } else {
            printf("Login failed. Exiting...\n");
            return -1;
        }
    } else {
        printf("Invalid argument. Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }

    return 0;
}
